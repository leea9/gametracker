import wmi
import pythoncom

# User imports
import utilities.Logging as logger

# Watches for start/exit of processes
# modes = ['creation', 'deletion']
def run_watcher(mode, exit_event):
    pythoncom.CoInitialize()
    try:
        c = wmi.WMI()
        process_watcher = c.Win32_Process.watch_for(mode)
        logger.log_info("Starting watcher with mode: {}".format(mode))

        while not exit_event.is_set():            
            try:
                new_process = process_watcher(timeout_ms=10)
            except wmi.x_wmi_timed_out:
                pass
            else:
                logger.log_info("{} {}".format(new_process.Caption, mode))
    finally:
        pythoncom.CoUninitialize()

# Polls the process list
def run_poller(exit_event):
    pythoncom.CoInitialize()
    try:
        c = wmi.WMI()
        while not exit_event.is_set():
            for proc in c.Win32_Process():
                if proc.Name == "notepad++.exe":
                    logger.log_info(proc.Name)
    except Exception as e:
        logger.log_error("Error: {}".format(e))
        exit_event.set()
    finally:
        pythoncom.CoUninitialize()