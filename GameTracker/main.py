# Python imports
from threading import Event, Thread
import sys

# User imports
import utilities.Logging as logger
import threadfunctions.WMI_funcs as WMI_funcs

exit_event = Event() # Used to signal threads to stop
def start_process_watchers():
    thread_list = []
    modes = ['creation', 'deletion']
    for m in modes:
        thread_list.append(Thread(target=WMI_funcs.run_watcher, args=(m, exit_event)))

    for t in thread_list:
        t.start()

    return thread_list

def start_poller():
    thread_poller = Thread(target=WMI_funcs.run_poller, args=(exit_event,))
    thread_poller.start()

    return thread_poller

def main_entry():
    thread_list = start_process_watchers()
    # thread_list.append(start_poller())

    try: # Main loop of program
        while not exit_event.is_set():
            stop = input("Enter x to stop: ")
            if stop == 'x':
                exit_event.set()

        # Wait for all threads to finish before exiting
        for t in thread_list:
            t.join()

        logger.log_info("All threads exited, shutting down")

    except KeyboardInterrupt:
        exit_event.set()

def initialize_ut():
    pass

if __name__ == "__main__":
    main_entry()