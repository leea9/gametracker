####################
#  Logging Levels  #
####################
# Critical         #
# Error            #
# Warning          #
# Info             #
# Debug            #
# NotSet           # 
####################

import logging
import os
import sys
import distutils.core

class Logging:
    """ Class to handle logging of application """
    class __Logging:
        def __init__(self, name, level="DEBUG", dir=None):
            self.logger = None
            self.log_level = level.upper()
            self.formatter = logging.Formatter('[%(asctime)s] %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
            self.log_dir = dir
            self.log_name = None

            if not self.log_dir:
                self.log_dir = os.getcwd() + r"\logs"

            self.setup_logger()

        def setup_logger(self):
            self.logger = logging.getLogger(self.log_name)
            self.logger.setLevel(self.log_level)

            # Create path to the log directory
            try:
                distutils.dir_util.mkpath(self.log_dir)
            except Exception as e:
                print("Could not create the specified directory: {}\n{}".format(self.log_dir, e))
                return False

            try:
                fh = logging.FileHandler(os.path.join(self.log_dir, 'MyFire.log'), mode='at', encoding=None)

                fh.setLevel(self.log_level)
                fh.setFormatter(self.formatter)
                self.logger.addHandler(fh) # Add outputting to a logfile
    
                sh = logging.StreamHandler(sys.stdout)
                sh.setLevel(self.log_level)
                sh.setFormatter(self.formatter)
                self.logger.addHandler(sh) # Add outputting to stdout
    
                self.logger.debug("Debug level set")
                self.logger.info("Logger initialized")
                return True
            except Exception as e:
                print("Could not create the log file: {}".format(e))
                return False

    instance = None
    def __init__(self, name, level="DEBUG", dir=None):
        if not Logging.instance:
            Logging.instance = Logging.__Logging(name, level, dir)

    def __getattr__(self, name):
        return getattr(self.instance, name)

# Global logger can be used anywhere
LOGGER = Logging('gametracker', 'DEBUG')

def log_info(message):
    LOGGER.instance.logger.info(message)

def log_debug(messag):
    LOGGER.instance.logger.debug(message)

def log_warn(message):
    LOGGER.instance.logger.warn(message)

def log_error(message):
    LOGGER.instance.logger.error(message)

def log_critical(message):
    LOGGER.instance.logger.critical(message)
