# Introduction #
GameTracker is a game time tracker and chat client based on the now discontinued Xfire. It will feature the ability to track your individual gaming hours based on what game you're currently playing.

Made with python 3.5.1 64-bit.  
Current Version: Development
---
### How do I get set up? ###
This file is a visual studio 2015 project which includes a solution file as well as a .proj file.  
This requires [python tools](http://microsoft.github.io/PTVS/) for visual studio to run the project.

To run the project, you will have to set up a virtual environment and install the dependencies below.
#### Dependencies ####
* Python 3.5.1 64-bit
* WMI
    * ```pip install wmi```
* pywin32 
     * ```pip install https://tr00st.co.uk/python/wheel/pywin32/pywin32-219-cp35-none-win_amd64.whl```
         * There might be an issue with the package throwing a dll missing error, if that is the case, follow the solution [here](http://stackoverflow.com/questions/7238403/import-win32api-error-in-python-2-6)

#### Running Project ####
Open up the solution file (MyFire.sln) and make sure your virtual environment is set up.  
Then just head over to Debug>Start Without Debugging/Start Debugging and the project will run.